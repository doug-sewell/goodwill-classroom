import React from 'react';
import Announcement from './Announcement';
import Countdown from './Countdown';

function Banner() {
    return (
        <div className="jumbotron jumbotron-fluid">
            <div className="container">
                <div className="wrapper bg-dark p-5">
                    <Announcement styleClass="display-4 text-white text-center">
                        There are only <Countdown days="16" hours="1" minutes="37" seconds="59" /> until final presentation
                    </Announcement>
                </div>
            </div>
        </div>
    )
}

export default Banner;