const assignments = [
    {
        imageSrc: 'https://placeimg.com/640/480/any',
        title: 'Html Tags by category',
        alt:'Html Tags by Category Assignment thumbnail',
        desc: 'Inline Elements vs Block Elements',
        createdOn: 'Sep 8th 2019',
        link: 'http://www.somesite.com/'
    },
    {
        imageSrc: 'https://placeimg.com/640/480/any',
        title: 'Get Started',
        alt: 'Get Started Assignment Thumbnail',
        desc: 'Button in html / css',
        createdOn: 'Sep 10th 2019',
        link: 'http://www.somesite.com/'
    }
];



function generateCards() {
    const cardElements = assignments.map(function (assignment) {
        return `<div class="card">
        <span class="icon">
            <i class="fas fa-heart"></i>
        </span>
        <img src="${assignment.imageSrc}" alt="${assignment.alt}" />
        <p class="title">${assignment.title}</p>
        <p class="title">${assignment.desc}</p>
        <p>Created on: ${assignment.createdOn}</p>
        <footer>
            <a href="" class="button button-orange">UpVote</a>
            <a class="button button-blue" href="${assignment.link}">View</a>
        </footer>
    </div>`;
    });
    
    const cardsElement = document.querySelector('.cards');
    cardsElement.innerHTML = cardElements.join(' ');
     
}



function createAssignmentCard(e) {
    e.preventDefault();

    const inputElements = document.querySelectorAll('.assignment-form input');
    
    const newAssignment = {
        imageSrc: 'https://placeimg.com/640/480/any',
        title: inputElements[0].value,
        desc: inputElements[1].value,
        createdOn: inputElements[2].value
    };

    assignments.push(newAssignment);
    generateCards();

    console.log('button clicked',inputElements);
};