const schedule = ['Jan 11 2018',
    'Feb 11 2018',
    'Mar 11 2018',
    'Apr 11 2018',
    'May 11 2018',
    'Jun 11 2018',
    'Jul 11 2018',
    'Aug 11 2018',
    'Sep 11 2018',
    'Oct 11 2018',
    'Nov 11, 2018',
    'Dec 11, 2018'];

console.log('Total number of dates:', schedule.length);

//console.log(schedule.length);

/*
INTERVIEW QUESTION: If you call for a variable element that is
more than the length of the array, an error will NOT be thrown.
Rather, it will return a value of undefined.

Example:
var num = [1,2,3];
console.log(num[3]);
undefined will be logged.
*/

/*
INTERVIEW QUESTION: Properties vs methods
Properties vs methods. 
Methods do not have parentheses.
Property example: Array.length;
Property method: Array.forEach();
*/

/**
 * First section: "let index = 0" - initialization
 * Second section: "index < array.length " - Decision Part
 * Third section: "index++" - increment part
 */
/*
console.log('Class Lecture:');
let weekends = [];

for (let index = 0; index < schedule.length; index++) {

    const currentSchedule = schedule[index];
    const dateObj = new Date(currentSchedule);
    const isHoliday = isWeekend(dateObj);

    if (isHoliday) {
        weekends.push(currentSchedule);
        index--;
        schedule.splice(index, 1);
    }

}
console.log('Weekends:', weekends, weekends.length);
console.log('Weekdays:', schedule, schedule.length);


console.log('********************');
*/

//Using forEach()
console.log('Using forEach()');
const weekendsForEach = [];
const weekdaysForEach = [];

schedule.forEach(date => {
    let dateObj = new Date(date);
    if (isWeekend(dateObj)) {
        weekendsForEach.push(date);
    } else {
        weekdaysForEach.push(date);
    }
});

console.log('Weekends: ', weekendsForEach, weekendsForEach.length);
console.log('Weekdays: ', weekdaysForEach, weekdaysForEach.length);

console.log('********************');

//using filter()
console.log('Using filter()')
const weekendsFilter = schedule.filter(day => {
    const dayObj = new Date(day);
    return isWeekend(dayObj);
});

const weekdaysFilter = schedule.filter(day => {
    const dayObj = new Date(day);
    return !isWeekend(dayObj);
});

console.log('Weekends: ', weekendsFilter);
console.log('Weekdays: ', weekdaysFilter);


console.log('********************');

//Using reduce()
console.log('Using reduce() and an object');

console.log(schedule.reduce((dayObj, date) => {
    const dateObj = new Date(date);
    if (isWeekend(dateObj)) {
        dayObj.weekends.push(date);
        return dayObj;
    } else {
        dayObj.weekdays.push(date);
        return dayObj;
    }
}, {
    weekends: [],
    weekdays: []
}));


console.log('******************');

//Using map()
console.log('Using map()');

//For cleaning up the arrays after using the map method:
const arrayCleaning = arr => {
    for (let x = 0; x < arr.length; x++) {
        if (arr[x] === undefined || arr[x] === NaN) {
            arr.splice(x, 1);
            x--;
        }
    }
    return arr;
}

let weekendsMap = schedule.map(date => {
    const dateObj = new Date(date);
    if (isWeekend(dateObj)) {
        return date;
    }
});

weekendsMap = arrayCleaning(weekendsMap);


let weekdaysMap = schedule.map(date => {
    const dateObj = new Date(date);
    if (!isWeekend(dateObj)) {
        return date;
    }
});

weekdaysMap = arrayCleaning(weekdaysMap);

console.log('Weekends: ', weekendsMap, weekendsMap.length);
console.log('Weekdays: ', weekdaysMap, weekdaysMap.length);