function isWeekend(date) {
    const dayNumber = date.getDay();
    if (dayNumber === 0 || dayNumber === 6) {
        return true;
    } else {
        return false;
    }
}