import React from 'react';
import './Footer.css'
function Footer() {

    return (
        <footer className="navbar navbar-light light-blue fixed-bottom">
            <p className="center m-auto">&copy; MissionCode {new Date().getFullYear()}</p>
        </footer>
    );
}

export default Footer;