import React from 'react';
import Nav from '../../shared/navigation/Nav';
import Footer from '../../shared/footer/Footer';
import Banner from './Banner';
import Announcement from './Announcement';
import Assignments from './Assignments';

function Home() {
  return (
    <main>
      <Nav />
      <section>
        <Banner />
        <Assignments />
      </section>
      <Footer />
    </main>
  )
}

export default Home;