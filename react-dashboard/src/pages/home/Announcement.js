import React from 'react';

function Announcement(props) {
    return(
        <div className={props.styleClass}>
            {
                props.children
            }
            
        </div>
    );
}

export default Announcement;