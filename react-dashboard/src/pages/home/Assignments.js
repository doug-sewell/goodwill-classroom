import React from 'react';
import Form from '../../shared/form/Form';

function Assignments() {
    const initialAssignments = [
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Html Tags by category",
            desc: "Inline elements vs Block Elements",
            createdOn: "Sep 8th 2019"
        },
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Get Started",
            desc: "Button in html / css",
            createdOn: "Sep 10th 2019"
        },
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Html Inline elements",
            desc: "Html Inline elements",
            createdOn: "Sep 14th 2019"
        }
    ];
    
    const [assignments, setAssignments] = React.useState(initialAssignments);

    const initialNewAssignment = {
        imageSrc: "https://placeimg.com/140/80/any",
        title: "",
        desc: "",
        createdOn: ""
    };

    const [assignmentFormVal, setAssignmentFormVal]= React.useState(initialNewAssignment);




    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-3">
                        <div className="card border-secondary mb-3">
                            <div className="card-header">New assignment Form</div>
                            <div className="card-body text-secondary">
                                <form onSubmit={handleFormSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="titleInput">Title</label>
                                        <input type="text" value={assignmentFormVal.title} onChange={handleTitleChange} className="form-control" id="titleInput" placeholder="Enter Assignment title" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="titleDesc">Description</label>
                                        <input type="text" className="form-control" id="titleDesc" placeholder="Enter Assignment description" value={assignmentFormVal.desc} onChange={handleDescChange} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="titleDate">Created Date</label>
                                        <input type="date" className="form-control" id="titleDate" placeholder="MM/DD/YYYY" value={assignmentFormVal.createdOn} onChange={handleDateChange} />
                                    </div>
                                    <button type="submit" className="btn btn-primary">Add assignment</button> 
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-9">
                        <div className="row">
                            {

                                assignments.map((assignment, index) => {
                                    return <div className="col-sm-12 col-md-6 col-lg-4" key={index}>
                                        <div className="card">
                                            <img src={assignment.imageSrc} className="card-img-top" alt="" />
                                            <div className="card-body">
                                                <h5 className="card-title">{assignment.title}</h5>
                                                <p className="card-text">Created on : {assignment.createdOn}</p>
                                                <div className="d-flex">
                                                    <a href="#" className="btn mr-auto btn-primary">UpVote</a>
                                                    <a href="#" className="btn btn-info">View</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                })

                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

    function handleFormSubmit($event){
        $event.preventDefault();
        // console.log("handle form submit", assignmentFormVal);
        const oldAssignments = assignments;
        const newAssignment = [assignmentFormVal]
        const newAssignments = oldAssignments.concat(newAssignment)
        setAssignments(newAssignments);
        setAssignmentFormVal(initialNewAssignment);
    }

    function handleTitleChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: $event.target.value,
            desc: assignmentFormVal.desc,
            createdOn: assignmentFormVal.createdOn
        })
    }

    function handleDescChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: assignmentFormVal.title,
            desc: $event.target.value,
            createdOn: assignmentFormVal.createdOn
        })
    }
    function handleDateChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: assignmentFormVal.title,
            desc: assignmentFormVal.desc,
            createdOn: $event.target.value
        })
    }

}



export default Assignments;