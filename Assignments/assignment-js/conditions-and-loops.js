//Used in Assignment One:
var now = new Date();

//Used in Assignment Two:
var someDate = new Date("Dec 28, 2018");

//Used in Assignment Three:
var schedule = ['Jan 11 2018',
                'Feb 11 2018',
                'Mar 11 2018',
                'Apr 11 2018',
                'May 11 2018',
                'Jun 11 2018',
                'Jul 11 2018',
                'Aug 11 2018',
                'Sep 11 2018',
                'Oct 11 2018',
                'Nov 11, 2018',
                'Dec 11, 2018'];
                
//Used in Assignemnt Four:
var range1 = 7;
var range2 = 121;

//Used in Assignments Five & Six
const orderedList = [13.98, 34.90, 44, 89.98, 234.23, 111.88];

//Function calls and ouput:
console.log('Assignment 1 Output:')
dayGreeting(now);
stars();
console.log('Assignment 2 Output:');
printDay(someDate);
stars();
console.log('Assignment 3 Output:');
countWeekends(schedule);
stars();
console.log('Assignment 4 Output:');
printRange(range1, range2);
stars();
console.log('Assignment 5 Output:');
console.log(findAverage(orderedList));
stars();
console.log('Assignment 6 Output:');
console.log('$' + findCosts(orderedList));

//Just for layout in separating the assignments in the console.
function stars() {
    console.log('********************');
}

//Assignment One Logic:
/*
    The only part minutes make a difference in the output is to check if it is noon.
    Beyond that, we look which range the hour falls into. If the hour is after 18:00 (6:00),
    then the night log statement prints.
*/
function dayGreeting(date) {
    const hour = date.getHours();
    const minute = date.getMinutes();

    if (hour < 12) {
        console.log('Good Morning');
    } else if (hour === 12 && minute === 0) {
        console.log('Good Noon');
    } else if (hour >= 12 && hour <= 15) {
        console.log('Good Afternoon');
    } else if (hour >= 16 && hour <= 18) {
        console.log('Good Evening');
    } else {
        console.log('Good Night');
    }
}

//Assignment Two Logic:
/*
    The logic checks if a year was passed in before logging the day.
    This is because days of the week vary by year.
    This funciton logs an error if no year was accompanied with the date.
*/
function printDay(date) {
    if (date.getYear()) {
        const day = date.getDay();
        //Logs the day as a word.
        switch (day) {
            case 0:
                console.log('Sunday');
                break;
            case 1:
                console.log('Monday');
                break;
            case 2:
                console.log('Tuesday');
                break;
            case 3:
                console.log('Wednesday');
                break;
            case 4:
                console.log('Thursday');
                break;
            case 5:
                console.log('Friday');
                break;
            case 6:
                console.log('Saturday');
                break;
            default:
                console.error('Something went wrong. We apologize for the inconvenience');
        }
    } else {
        console.error('No year was given. Please try again with a year provided.');
    }
}

//Assignment Three Logic:
/*
    Loop through the array of dates passed in. 
    If the day is Saturday/Sunday, the counter variable increases by one.
    Because dates may be weekends in different years, the function keeps track if no year was passed in.
    At the end, we display the number of weekends. If any dates passed in did not have a year specified, the console lets the user know.
*/
function countWeekends(dates) {
    var counter = 0;
    var weekendError = 0;

    for (var x = 0; x < dates.length; x++) {
        const date = new Date(dates[x]);
        const day = date.getDay();

        if (day === 0 || day == 6) {
            counter += 1;
        }
        if (!date.getYear()) {
            weekendError++;
        }
    }
    if (counter > 0) {
        console.log(`There are ${counter} weekends.`);
    } else {
        console.log('No weekends');
    }
    if (weekendError > 0) {
        console.info('Please note that at least one of the dates provided did not have a year given. For the most accurate results, please provide a year with all dates.');
    }
}

//Assignemnt 4 Logic:
/*
    If the first number is less than the second number, the numbers are listed in ascending order.
    If the first number is greater than the second number, the numbers are listed in descending order.
    If equal to each other, there is no range. thus we let the user know there is no range.
*/
function printRange(num1, num2) {
    if (num1 < num2) {
        for (var x = num1; x <= num2; x++) {
            console.log(x);
        }
    } else if (num1 > num2) {
        for (var x = num1; x >= num2; x--) {
            console.log(x);
        }
    } else if (num1 === num2) {
        console.log('No range. Both numbers are equal.');
    } else {
        console.error('Something went wrong.');
    }
}

//Assignment 5 Logic:
/*
    Loops through arguments, adding each number to total.
    Then returns the value divided by the amount of numbers which is the average.
*/
function findAverage(args) {
    var total = 0;

    for (var x = 0; x < args.length; x++) {
        total += args[x];
    }
    return total / args.length;
}

//Assignment 6 Logic:
/*
    Each variable calls a function to get its value so that the function is broken up 
    into smaller chunks of code that can be reused in other situations.
*/
function findCosts(baseItemPrices) {
    const tax = .05;
    var subTotal = findSubTotal(baseItemPrices);
    var taxTotal = findTax(subTotal, tax);
    var total = findFinalPrice(subTotal, taxTotal);
    return total;
}

function findSubTotal(prices) {
    var total = 0;
    for (var x = 0; x < prices.length; x++) {
        total += prices[x];
    }
    return total;
}

function findTax(subTotal, tax) {
    return subTotal * tax;
}

function findFinalPrice(subTotal, tax) {
    var total = Math.round((subTotal + tax) * 100) / 100;
    return total;
}