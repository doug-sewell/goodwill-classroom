import React from 'react';


function Form() {


    const initialAssignments = [
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Html Tags by category",
            desc: "Inline elements vs Block Elements",
            createdOn: "Sep 8th 2019"
        },
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Get Started",
            desc: "Button in html / css",
            createdOn: "Sep 10th 2019"
        },
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Html Inline elements",
            desc: "Html Inline elements",
            createdOn: "Sep 14th 2019"
        }
    ];
    
    const [assignments, setAssignments] = React.useState(initialAssignments);

    const initialNewAssignment = {
        imageSrc: "https://placeimg.com/140/80/any",
        title: "",
        desc: "",
        createdOn: ""
    };

    const [assignmentFormVal, setAssignmentFormVal]= React.useState(initialNewAssignment);



    return(
        <form onSubmit={handleFormSubmit}>
        <div className="form-group">
            <label htmlFor="titleInput">Title</label>
            <input type="text" value={assignmentFormVal.title} onChange={handleTitleChange} className="form-control" id="titleInput" placeholder="Enter Assignment title" />
        </div>
        <div className="form-group">
            <label htmlFor="titleDesc">Description</label>
            <input type="text" className="form-control" id="titleDesc" placeholder="Enter Assignment description" value={assignmentFormVal.desc} onChange={handleDescChange} />
        </div>
        <div className="form-group">
            <label htmlFor="titleDate">Created Date</label>
            <input type="date" className="form-control" id="titleDate" placeholder="MM/DD/YYYY" value={assignmentFormVal.createdOn} onChange={handleDateChange} />
        </div>
        <button type="submit" className="btn btn-primary">Add assignment</button> 
    </form>
    )

    function handleFormSubmit($event){
        $event.preventDefault();
        // console.log("handle form submit", assignmentFormVal);
        const oldAssignments = assignments;
        const newAssignment = [assignmentFormVal]
        const newAssignments = oldAssignments.concat(newAssignment)
        setAssignments(newAssignments);
        setAssignmentFormVal(initialNewAssignment);
    }

    function handleTitleChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: $event.target.value,
            desc: assignmentFormVal.desc,
            createdOn: assignmentFormVal.createdOn
        })
    }

    function handleDescChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: assignmentFormVal.title,
            desc: $event.target.value,
            createdOn: assignmentFormVal.createdOn
        })
    }
    function handleDateChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: assignmentFormVal.title,
            desc: assignmentFormVal.desc,
            createdOn: $event.target.value
        })
    }

}

export default Form;