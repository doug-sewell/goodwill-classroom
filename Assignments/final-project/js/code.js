/* 
README:
The following code was done after a very large refactor. 
Further work needs to be done for code organization for more organized data flow as well as implement better modularity. 
However, this is a working prototype. 
Code is not final and has bugs still to be ironed out.

TO DO:
- Bug Fix: Remove event handler for the enter key after it's pressed.
- Creating the "dummy" jobs still has some old code as a dependency. Can't just be removed. Needs to be refactored.
- The page uses a standard browser alert if the user does a blank search. Replace browser alert with a modal for better UX.
*/

{

    //UI
    const uiManipulation = () => {
        displayHome = () => {
            document.getElementById('main-content').innerHTML = `
        <section class="d-flex justify-content-center align-items-center flex-column w-auto min-vh-100" id="home">
        <h1 class="sr-only">Job Search</h1>
        <img src="assets/mission-code-logo.png" alt="Mission Code Logo">
        <form class="w-100">
            <div class="form-row align-items-center m-0">
                <div class="w-100 d-flex justify-content-center">
                    <label class="sr-only" for="search-bar">Job Search</label>
                    <div class="input-group mb-2 d-flex justify-content-center w-75">
                        <input type="text" class="form-control rounded-0 border-right-0 pt-4 pb-4"
                            id="search-bar"
                            aria-label="Search for job postings by keyword, job title, or company"
                            placeholder="Search for job postings by keyword, job title, or company">
                        <div class="input-group-append">
                            <span class="input-group-text border-left-0 bg-white rounded-0"
                                id="search-bar_magnifying-glass"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
        `;
        }

        displayJobs = (jobs) => {
            let html;

            if (jobs.length > 0) {
                html = `<div class="container-fluid" id="job-postings">
                    <div class="d-flex align-items-center flex-column custom-header-footer-content-clear-margin">
                    `;
                for (let x = 0; x < jobs.length; x++) {
                    html += `
                 <div class="w-75 text-center border border-dark mt-3 custom-job-option" data-index-number=${x} data-job-title="${jobs[x].title}" data-job-description="${jobs[x].description}">
                     <p class="m-0">${jobs[x].title}<br>
                         ${jobs[x].description}
                     </p>
                 </div>
             `
                }
                //This was likely meant to only display three jobs or so and this icon would show the rest. 
                //Unsure, so for now just placing at the end of job listings:
                html += `<p class="mt-3 h1" id="three-dot-menu"><i class="fas fa-ellipsis-v"></i></p>`; 

            } else {
                html = `<div class="container-fluid" id="job-postings">
                    <div class="d-flex align-items-center justify-content-center flex-column custom-header-footer-content-clear-margin min-vh-100 text-center">
                        <p class="h3">There aren't any openings at this time.<br>Please check back soon and thank you for your interest!</p>
                    `;
            }
            html += `</div>
        </div>
        `;
            document.getElementById(`main-content`).innerHTML = html;
        }

        displayJobApplication = (title, description) => {
            let html = `<section class="d-flex justify-content-center align-items-center w-auto min-vh-100" id='job-applicant-ui'>
        <h2 class="sr-only">Job Application Form</h2>
        <form class=" w-75">
            <div class="container-fluid text-center" id="job-information">
                <p>${title}<br>
                    ${description}
                </p>
            </div>
            <div class="form-group row">
                <label for="input-name" class="col-sm-3 col-form-label text-right">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="input-name">
                </div>
            </div>
            <div class="form-group row">
                <label for="input-address" class="col-sm-3 col-form-label text-right">Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="input-address">
                </div>
            </div>
            <div class="form-group row">
                <label for="input-email" class="col-sm-3 col-form-label text-right">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="input-email">
                </div>
            </div>
            <div class="form-group row">
                <label for="input-phone" class="col-sm-3 col-form-label text-right">Phone Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="input-phone">
                </div>
            </div>
            <div class="form-group row">
                <label for="resume-upload" class="col-sm-3 col-form-label text-right pt-3 pb-3">Resume*</label>
                <div class="custom-file col-sm-9">
                    <input type="file" class="d-none" id="resume-upload">
                    <label for="resume-upload" class="btn btn-primary pt-3 pb-3">Upload File</label>
                </div>
            </div>
        </form>
    </section>`
            document.getElementById(`main-content`).innerHTML = html;
        }

        lookForUserJobClick = () => {
            const jobs = document.querySelectorAll('.custom-job-option');
            jobs.forEach(job => {
                job.addEventListener('click', e => {
                    displayJobApplication(e.currentTarget.dataset.jobTitle, e.currentTarget.dataset.jobDescription);
                });
            });
        }

        resetSearchEventListener = () => {

            const performSearch = () => {
                let query = userSearchQuery();
                if (!query) {
                    alert('A valid search query is required. Please type in a desired job.');
                } else {
                    let newJobs = createFilteredJobsArray(jobs, query);
                    displayJobs(newJobs);
                    lookForUserJobClick();
                }

            }

            document.getElementById('search-bar_magnifying-glass').addEventListener('click', () => {
                performSearch();
            });

            document.addEventListener('keydown', (e) => {
                if (e.keyCode === 13) {
                    performSearch();
                }
            });
        }

        userSearchQuery = () => {
            return document.getElementById('search-bar').value.trim(); //The trim eliminates searches with only spaces.
        }

    }

    //JOB POSTING MANIPULATION
    const jobManipulation = () => {
        class Job {
            constructor(title, description) {
                this.title = title;
                this.description = description;
                return this;
            }
        }

        const createNewJob = (title, description) => {
            let job = new Job(title, description);
            return job;
        }

        createDemoDevJobs = (jobs) => {
            jobs.push(createNewJob('Front End Web Dev', 'Know HTML, CSS, and JavaScript'));
            jobs.push(createNewJob('QA', 'Debug and fix code'));
            jobs.push(createNewJob('Marketer', 'Market the new web project.'));
            jobs.push(createNewJob('Software Developer', 'Be an expert in Java'));
            jobs.push(createNewJob('Android Developer', 'Be an expert in Java, Kotlin, React Native, or Native Script'));
            jobs.push(createNewJob('iOS Developer', 'Be an expert in Swyft'));
            jobs.push(createNewJob('Back End Developer', `Be an expert in Node.JS, Java, C#, PHP, or other backend language and/or framework.`));
            jobs.push(createNewJob('Full Stack Developer', 'See requirements of Front End and Back End Developer roles'));
            jobs.push(createNewJob('Software Developer Utilizing Web Technologies', 'Utilize front end technologies to create MacOS and Windows 10 software using Electron.'));
            jobs.push(createNewJob('React Native Developer', 'Create Native mobile applications for both Android and iOS utilizing React Native'));
            jobs.push(createNewJob('Material Design Web Designer', `Understand Google's best practices for Material Design to create responsive layouts`));
            jobs.push(createNewJob('Kotlin Developer', 'Be familiar with JVM and core Kotlin concepts.'));
            jobs.push(createNewJob('JavaScript Engineer', 'Expert in front-end development, including JSON, objectect oriented programming (OOP)'));
            jobs.push(createNewJob('PHP Developer', 'Create server-side code utilizing PHP.'));
            jobs.push(createNewJob('Figma Web Designer', 'Utilize Figma for rapid prototyping'));
            jobs.push(createNewJob('Adobe XD Designer', 'Utilize Adobe XD for rapid prototyping'));
            jobs.push(createNewJob('CSS Designer', 'Utilize CSS for creative web design'));
            jobs.push(createNewJob('Web Accessibility Engineer', 'Know all aspects of WCAG 2.1 guidelines for web accessibility engineering'));
            jobs.push(createNewJob('UX Researcher', 'Conduct studies on UX for improvement of enterprise services'));
            jobs.push(createNewJob('Angular Developer', 'Utilize cutting edge front end technologies and leverage the Angular framework for UI development.'));
            jobs.push(createNewJob('React Developer', 'Utilize cutting edge front end technologie and leverage the React library for UI development'));
            jobs.push(createNewJob('Vue Developer', `Utilize cutting edge front end technologies and leverage the Vue framework for UI development`));
            jobs.push(createNewJob('Progressive Web App (PWA) Developer', `Update legacy web projects into PWA's as well as develop new PWA's for clients`));
            jobs.push(createNewJob('Firebase Full Stack Developer', `Utilize HTML, CSS, and JavaScript with Firebase for full stack development.`));
            return jobs;
        }

        createFilteredJobsArray = (jobs, userSearch) => {
            let filteredJobs = [];
            let checkedUserSearch = userSearch.toLowerCase().trim();


            filteredJobs = jobs.filter(job => {
                let checkedJobInfo = new Job(job.title.trim().toLowerCase(), job.description.trim().toLowerCase());

                if (checkedJobInfo.title.includes(checkedUserSearch) || checkedJobInfo.description.includes(checkedUserSearch)) {
                    return true;
                }
            });
            return filteredJobs;
        }

        return {
            createNewJob: (title, description) => {
                let job = createNewJob(title, description);
                return job;
            },
            createDemoDevJobs: (jobs) => {
                createDemoDevJobs(jobs);
                return jobs;
            }
        }
    }

    //INIT
    const init = () => {
        jobManipulation();
        uiManipulation();
        displayHome();
        resetSearchEventListener();

        document.getElementById('display-jobs').addEventListener('click', () => {
            displayJobs(jobs);
            lookForUserJobClick();
        });

        document.getElementById('logo').addEventListener('click', () => {
            displayHome();
            resetSearchEventListener();
        });
    }


    //DATA
    init(); //initializes app
    let jobs = []; //Holds job objects for manipulation. 
    jobs = createDemoDevJobs(jobs); //WILL NOT BE IN PRODUCTION. This creates dummy listings to mess with.
}