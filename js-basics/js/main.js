//Const is keyword. When value is not changing, we have to use keyword const.


/*
New operator will create new object of class {Date}
*/

//2 & 4 are tuessday and thursday
const webDevClasses = [2,4];
// 1, 3, 5  -
const testingClass = [1,3,5];


const currentDate = new Date();
//This is function call or execution
printMessage('Current Date is: ' + currentDate);
printMessage('Current Date is: ' + currentDate, 'info');
printMessage('Current Date is: ' + currentDate, 'error');

const halloweenWeekend = new Date(2019, 10, 2, 0, 0, 0, 0);
const isHoliday = isWeekend(currentDate);



if (isHoliday === true) {
    printMessage('Store is closed', 'error');
} else {
    printMessage('Store is open');
}


const classDay = isClassDay(currentDate);


if (classDay) {
    printMessage('Today is a web dev class day', 'info')
} else {
    printMessage('Today is not a web dev class day', 'error');
}

if(classDay) {
    classTimeSession(currentDate);
}


//print error log message wehn type is error
//print info logs message when type is info

/**
 * print the message
 * @param {string} message 
 */

//This is function definition 
function printMessage(message, type) {
    if (type === 'info') {
        console.info(message);
    } else if (type === 'error') {
        console.error(message);
    } else {
        console.log(message);
    }
}

// Find weekend based on the date.
/**
 * When day is sunday or saturday, it's the weekend
 */

/**
 * 
 * @param {Date} date 
 * returns boolean 
 */

function isWeekend(date) {
    const dayNumber = date.getDay();
    if (dayNumber === 0 || dayNumber === 6) {
        return true;
    } else {
        return false;
    }
}



/**
 * 
 * @param {Date} date 
 * returns boolean
 */
function isClassDay(date,openDays) {
    const dayNumber = date.getDay();

    if(openDays.indexOf(dayNumber)) {
        return true;
    } else {
        return false;
    }



}

/**
 * 
 * @param {Date} date
 * returns boolean 
 */
function classTimeSession(date) {
    const hours = date.getHours();
    const minutes = date.getMinutes();

    if((hours === 18 && minutes >= 30) || (hours === 20 && minutes <= 30)) {
        return true;
    }
        return false;
}












function isClassOpen() {
    const now = new Date();


    const day = isClassDay(now, webDevClasses);
    const time = classTimeSession(now);

    

    if(day && time) {
        console.log('class is officially on!');
    } else {
        console.log('The class is not in session');
    }
}

isClassOpen();

/* Started working, but didn't finish. This returns an error. 

function isTodayClass(date) {
    const isClassDay = isClassDay(date);
    if(isClassDay) {
        const isTime = classTimeSession(date);
        if(isTime) {
            printMessage('Class is in session!');
        }
    } else {
        printMessage('Class is not in session');
    }
}
*/




//6:30 - 8:30 class in progress

/*
traversing => Going through each element in an array.

Top used array methods:
forEach()

*/